/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.arraylistmanual;
import java.util.ArrayList;
public class ArraylistManual {

    public static void main(String[] args) {
      
        ArrayList arlTest = new ArrayList();

        System.out.println("Tamaño de arrayList en la creacion: " + arlTest.size());
        arlTest.add("D");
        arlTest.add("U");
        arlTest.add("K");
        arlTest.add("E");

        System.out.println("Tamaño de arrayList DESPUES DE AGREGAR ELEMENTOS: " + 
        arlTest.size());

        System.out.println("Lista de todos los elementos: " + arlTest);

        arlTest.remove("D");

        System.out.println("Ver contenido despues de eliminar un elemento (D): " +
        arlTest);
        System.out.println("Tamaño de arrayList DESPUES DE ELIMINAR ELEMENTOS: " +
        arlTest.size());
        //Para verificar si en la lista contiene algun elemento "K".
        System.out.println(arlTest.contains("K"));
    }
}


