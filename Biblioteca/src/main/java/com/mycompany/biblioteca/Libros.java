/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.biblioteca;

/**
 *
 * @author MVentura
 */
public class Libros implements PretaLibro {
   private String  codigo;
    private String titulo;
    private String estado;
   private int año; 
   private boolean prestado;
   
   public Libros(String c, String t, String e, int a, boolean p){
       this.año=a;
       this.codigo=c;
       this.estado=e;
       this.titulo=t;
       this.prestado= false;
       
   }
   public String getCodigo() {
       return this.codigo;
   }
   public int getAño(){
       return this.año;
   }
   @Override
   public String toString() {
       return  "Titulo: " + this.titulo +"\n"+ "Codigo: " + this.codigo+"\n" + "Año: " + (this.año)+"\n" + "Estado: " +  this.estado+"\n" + "Prestado: " + (this.prestado);
       
   
   }
  
   @Override
    public void prestar() {
        this.prestado = true;
    }

    @Override 
    public void devolver() {
        this.prestado=false;

    }

    
   @Override
    public boolean prestado() {
        return this.prestado;
        
    }
}

