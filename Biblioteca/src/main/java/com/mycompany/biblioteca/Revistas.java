/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.biblioteca;

/**
 *
 * @author MVentura
 */
public class Revistas  implements PrestaRevista{
     private String  codigo;
    private String titulo;
    private String estado;
   private int año; 
   private int numero;
   
   public Revistas(String c, String t, String e, int a, int n){
       this.codigo=c;
       this.titulo=t;
       this.estado=e;
       this.numero=n;
       this.año=a;
   }
   @Override
   public String toString() {
       return  "Titulo: " + this.titulo+"\n" + "Codigo: " + this.codigo+"\n" + "Año: " + (this.año)+"\n" + "Estado: " +  this.estado+"\n" + "numero " + (this.numero);
       
   }
   public String getCodigo() {
       return this.codigo;
   }
   public int getAño(){
       return this.año;
   }
@Override
    public void prestar() {
        
    }

    @Override 
    public void devolver() {
        

    }

    
   @Override
    public int numero() {
        return this.numero;
        
    }
}
