/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.buclefor;

/**
 *
 * @author MVentura
 */
public class For {
   
    public static void for1(){
         System.out.print("ESTRUCTURA DE CONTROL FOR: -");
        
        for (int num=2; num<=100; num=num + 2){
            System.out.print(num+"-");
        }
    }
    
}
