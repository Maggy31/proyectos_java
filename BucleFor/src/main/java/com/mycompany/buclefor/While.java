/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.buclefor;

/**
 *
 * @author MVentura
 */
public class While {
    public static void while1(){
        int num=0;
        System.out.println();
        System.out.print("ESTRUCTURA DE CONTROL WHILE: -");
        while(num <100){
            num=num+2;
            
            System.out.print(num+"-");
        }
    }
    
}
