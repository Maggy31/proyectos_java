/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.claseabstract;

/**
 *
 * @author MVentura
 */
public class ClaseAbstract {

    public static void main(String[] args) {
       Flores flores1=new Flores("rosa","roja","mediana");
        Flores flores2=new Flores("bugambilia","morada","grande");
       Flores flores=new Flores("cuna de moises","blanca","mediana");
       System.out.println("flores:"+"\n"+flores1.datos()+"\n"+ flores2.datos()+"\n"+ flores.datos()+"\n");
       
       System.out.println(flores1.contarPlantas());
    }
}
