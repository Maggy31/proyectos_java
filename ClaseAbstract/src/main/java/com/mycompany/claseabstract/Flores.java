/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.claseabstract;

/**
 *
 * @author MVentura
 */
public class Flores extends Plantas implements PlantaInt{
     public static int contarEspecies;
    private String  especieFlor;
    
    public Flores(String especieFlor, String color, String tamaño) {
        super(color, tamaño);
        this.especieFlor = especieFlor;
        Flores.contarEspecies++;
    }
    public String datos(){
        return "flores"+"\n"+"especie de la flor: "+ this.especieFlor+"\n"+"su color es: "+ getColor()+"\n"+"el tamaño es: "+getTamaño()+"\n";
    }
    public int contarPlantas(){
       return contarEspecies;
    }
    
    
}

