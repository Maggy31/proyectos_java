/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.claseabstract;

/**
 *
 * @author MVentura
 */
public abstract class Plantas {
   
    private String color;
    private String tamaño;

    public Plantas(String color, String tamaño) {
        
        this.color = color;
        this.tamaño = tamaño;
    }

   

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }
    
    public abstract String datos();
    
}
