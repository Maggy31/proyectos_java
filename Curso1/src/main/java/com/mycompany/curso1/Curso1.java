/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.curso1;

import java.util.Hashtable;
import java.util.Scanner;

/**
 *
 * @author MVentura
 */
public class Curso1 {

    public static void main(String[] args) {
      //un comentario corto
      /*
      varios comentarios en lineas diferentes
      */
      //variables
      int numero=23;
      String nombre ="Margarita";
      int primera=10;
      //fija
              primera++;
              //prefija
              ++primera;
              System.out.println(primera);
              
              System.out.println(numero  +  nombre);
              
              //estructuras de conjtrol condicional
              int num=100;
              if(num==99){
                  System.out.println("es correcto");
              }else{
              System.out.println("es incorrecto");
              }
              //el switch es igual al if
              int primera1=5;
              switch(primera1){
                  case 1:
                      System.out.println("es uno");
                      break;
                  case 2: 
                      System.out.println("es dos");
                      break;
                  case 3:
                      System.out.println("es tres");
                      break;
                  case 4:
                      System.out.println("es cuatro");
                      break;
                  case 5:
                      System.out.println("es cinco");
                      break;
                  default:
                      System.out.println("no existe");
                      break;
                             
                      
                  
              }
              //excepcones
              try{
                  int uno=4/0;
              }catch(Exception e){
                  System.out.println(e);
              }
              //estructura de control repetidas
              int uno=10;
              while(uno >5){
                  System.out.println(uno);
                  uno--;
              }
              do{
                  System.out.println(uno);
                  uno++;
              
              }while(uno<5);
              System.out.println("es incorrecto");
              
              String[]semana={"lunes","martes","miercoles","jueves","viernes","sabado","domingo"};
              for(String a:semana){
                  System.out.println(a);
              }
              for(int u=1; u<10; u++){
                  System.out.println(u);
              }
              
              for(int x=1; x<=10; x++){
                  if(x>=5&& x<=7){
                      continue;
                  }
                      System.out.println(x);
                  
              }
              //array unidimencional
         
              String[]semana1={"lunes","martes","miercoles","jueves","viernes","sabado","domingo"};
          System.out.println(semana1[3]);
//otra manera de crear arreglos
              String[]semanas=new String[7] ;
              semanas[0]="lunes";
               semanas[1]="martes";
                semanas[2]="miercoles";
                 semanas[3]="jueves";
                  semanas[4]="viernes";
                   semanas[5]="sabado";
                    semanas[6]="domingo";
                    System.out.println(semanas[4]);
                    
      Hashtable <Integer, String> semana2=new Hashtable<Integer, String>();
        semana2.put(22, "lunes");
        semana2.put(2, "martes");
        semana2.put(12, "miercoles");
        semana2.put(2, "jueves");
        semana2.put(3, "viernes");
        semana2.put(4, "sabado");
        semana2.put(8, "domingo");
        System.out.println(semana2.get(8));
                  
      
 //PEDIR AL USUARIO TRES NUMEROS Y LOS MUESTRE ORDENADOS DE MENOR A MAYOR
 //PEDIR UN NUMERO ENTRE EL CERO Y 10000 Y DECIR CUANTAS CIFRAS TIENE
//PEDIR UNA NOTA DEL CERO AL 10 Y MOSTRARLA DE LA FORMA: INSUFICIENTE SUFICIENTE BIEN NOTABLE SOBRE SALIENTE.

Scanner entrada =new Scanner (System.in);






}
    
    
}
