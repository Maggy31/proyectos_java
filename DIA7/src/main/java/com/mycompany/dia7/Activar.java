/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.dia7;

/**
 *
 * @author MVentura
 */
public interface Activar {
    
    
    void activar();
    void desactivar();
    boolean isActivo();
    public double compareTo(Object a);
   
}

    

