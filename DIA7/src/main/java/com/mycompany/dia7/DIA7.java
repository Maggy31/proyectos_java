/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.dia7;

/**
 *
 * @author MVentura
 */
public class DIA7 {

    public static void main(String[] args) {
       Pelicula [] aPeliculas = new Pelicula[5];
        Pelicula pelicula1 = new Pelicula("spirit", "Lorna Cook",3);
        aPeliculas[0]= pelicula1;
        Pelicula pelicula2 = new Pelicula("El barco fantasma", 3, "Terror", "Steve Beck ");
        aPeliculas[1] = pelicula2;
        Pelicula pelicula3 = new Pelicula("La chica salvaje", "Olivia Newman", 2);
        aPeliculas[2] = pelicula3;
        Pelicula pelicula4 = new Pelicula("El rey escorpion", 4, "accion", " Chuck Russel");
        aPeliculas[3] = pelicula4;
        Pelicula pelicula5 = new Pelicula("la monja", 5,"Terror","Corin Hardy");
        aPeliculas[4] = pelicula5;

        Videojuego [] aVideojuegos = new Videojuego[5];
        Videojuego videojuego1 = new Videojuego("Mario Kart 8",6,"Nintendo");
        aVideojuegos[0]= videojuego1;
        Videojuego videojuego2 = new Videojuego("Halo:Combat evolved", 4, true, "Disparo", "Bungie Studios");
        aVideojuegos[1] = videojuego2;
        Videojuego videojuego3 = new Videojuego(false,"control", "Remedy Entertainment");
        aVideojuegos[2] = videojuego3;
        Videojuego videojuego4 = new Videojuego("God of Wa", 5, "Sony Computer");
        aVideojuegos[3] = videojuego4;
        Videojuego videojuego5 = new Videojuego("Splatoon 3", 3, true, "disparos","Nintendo");
        aVideojuegos[4] = videojuego5;

        pelicula2.activar();
        pelicula5.activar();
       
        videojuego5.activar();
        videojuego1.activar();

        //int mayor = arregloPeliculas[0].getDuracion();
        Pelicula MDuración = new Pelicula();
        Videojuego MasJugado = new Videojuego();
        int activaPeli = 0;
                for(int a=0; a < aVideojuegos.length;a++){
                    if(aVideojuegos[a].estatus == true){  
                        activaPeli += 1;
                    }else{
                        System.out.println("Videojuegos no activos: " + aVideojuegos[a].toString());
                    }
                    if(aVideojuegos[a].getDuracion() > MasJugado.getDuracion()){
                        MasJugado = aVideojuegos[a];
                    }
                }                
                for(int z=0; z < aPeliculas.length;z++){                   
                    if(aPeliculas[z].getGenero() == "Terror" && aPeliculas[z].getDuracion() > MDuración.getDuracion()){    
                        MDuración = aPeliculas[z];   
                    }  
                        if(aPeliculas[z].estatus == true){
                            activaPeli += 1;
                            
                        }else{
                            System.out.println("Peliculas no activas: " + aPeliculas[z].toString());
                        }

                    }                   
                    System.out.println("Videojuego mas jugado: " + MasJugado.toString());
                    System.out.println("Pelicula con mayor duracion: " + MDuración.toString());
                    System.out.println("El numero de peliculas y videojuegos activas son: " + activaPeli); 
                    System.out.println(aPeliculas[3].compareTo(aVideojuegos[3])+ " horas");
                }                               
            }           

      

