/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dia7;

/**
 *
 * @author MVentura
 */
public class Pelicula implements Activar {
    private String titulo;
    private Integer duracion=2;
    public boolean estatus= false;
    private String genero;
    private String director;
     public Pelicula(){
      
  }

    public Pelicula(String titulo, Integer duracion, boolean estatus, String genero, String director) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.estatus = estatus;
        this.genero = genero;
        this.director = director;
    }

    public Pelicula(String titulo, String director, Integer duracion) {
        this.titulo = titulo;
        this.director = director;
        this.duracion=duracion;
    }

   public Pelicula(String titulo, Integer duracion, String genero, String director) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.genero = genero;
        this.director = director;
    }
  public Pelicula(String titulo, String genero, String director) {
        this.titulo = titulo;
        this.genero = genero;
        this.director = director;

       
   }
 

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return  "titulo=" + titulo+"\n" + ", duracion=" + duracion+"\n" + ", estatus=" + estatus+"\n"+ ", genero=" + genero +"\n"+ ", director=" + director;
    } 
    
    @Override
    public double compareTo(Object a) {
        Videojuego v = Videojuego.class.cast(a);
        return this.getDuracion() - v.getDuracion();
    }


    @Override
    public void activar() {
     this.estatus=true; 
    }

    @Override
    public void desactivar() {
    this.estatus=false;
    }

    @Override
    public boolean isActivo() {
     return this.estatus;  
    }

}
    
    
    

