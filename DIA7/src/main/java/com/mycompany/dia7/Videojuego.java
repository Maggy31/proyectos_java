/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dia7;

/**
 *
 * @author MVentura
 */
public class Videojuego implements Activar{
    private String titulo;
    private Integer duracion=10;
    public boolean estatus;
    private String genero;
    private String compañia;

    public Videojuego(String titulo, Integer duracion, boolean estatus, String genero, String compañia) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.estatus = estatus;
        this.genero = genero;
        this.compañia = compañia;
    }

    public Videojuego(boolean estatus, String titulo, String compañia) {
        this.estatus = estatus;
        this.genero = titulo;
        this.compañia = compañia;
    }

    public Videojuego(String titulo, Integer duracion, String compañia ) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.compañia = compañia;
    }
    public Videojuego(){
        
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCompañia() {
        return compañia;
    }

    public void setCompañia(String compañia) {
        this.compañia = compañia;
    }

   

    @Override
    public String toString() {
        return "Videojuego{" + "titulo=" + titulo + ", duracion=" + duracion + ", estatus=" + estatus + ", genero=" + genero + ", compa\u00f1ia=" + compañia + '}';
    }
     public double compareTo(Object a) {
        Pelicula p = Pelicula.class.cast(a);
        return this.getDuracion() - p.getDuracion();
    }


    @Override
    public void activar() {
      this.estatus=true;
    }

    @Override
    public void desactivar() {
      this.estatus=false;
    }

    @Override
    public boolean isActivo() {
       return this.estatus; 
    }

   

   
    
    
}
