/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.ejemploexcepcion;
    import java.util.Scanner;
import java.util.InputMismatchException;




public class EjemploExcepcion {

    public static void main(String[] args) {
      
        try{
            Scanner ejem = new Scanner(System.in);
            System.out.println("Introduzca un numero");
            int num1 = ejem.nextInt();
            int num2 = ejem.nextInt();
  
            System.out.println("Resultado = " + (num1/num2));
        }
        //Si el valor introducido por teclado no es del tipo esperado, se produce un error.y se lanza la excepción InputMismatchException
        catch(InputMismatchException e){
            System.out.println("No ingresaste un número válido. "+e);
        }
        //se genera cuando intentas dividir in numero entre cero
        catch(ArithmeticException e){
            System.out.println("ocurrio una excepcion aritmetica: " + e);
        }
      
    }
}