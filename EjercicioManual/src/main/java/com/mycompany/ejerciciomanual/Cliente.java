/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ejerciciomanual;

/**
 *
 * @author MVentura
 */
public class Cliente {
    private String nombre;
    private Double cantidadDinero;

    public Cliente(String nombre, Double cantidadDinero) {
        this.nombre = nombre;
        this.cantidadDinero = cantidadDinero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getCantidadDinero() {
        return cantidadDinero;
    }

    public void setCantidadDinero(Double cantidadDinero) {
        this.cantidadDinero = cantidadDinero;
    }
    
    
}
