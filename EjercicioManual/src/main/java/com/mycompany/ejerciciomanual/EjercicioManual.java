/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.ejerciciomanual;

/**
 *
 * @author MVentura
 */
public class EjercicioManual {

    public static void main(String[] args) {
        Producto jabon= new Producto("jabon",150) ;
        Cliente cliente1=new Cliente("luis",20.50);
        Tienda tienda =new Tienda("sonora", "calle #45", jabon, cliente1);
        System.out.println("nombre del cliente: " + tienda.getCliente().getNombre());
        System.out.println("nombre del producto: " +tienda.getProducto().getNombre());
    }
}