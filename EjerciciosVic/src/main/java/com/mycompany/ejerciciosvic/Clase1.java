/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ejerciciosvic;


import java.util.Scanner;

/**
 *
 * @author MVentura
 */
public class Clase1 {
    public static void num(){
        //PEDIR AL USUARIO TRES NUMEROS Y LOS MUESTRE ORDENADOS DE MENOR A MAYOR
        Scanner entrada = new Scanner (System.in);
        try{
        System.out.println("ingresa el primer  numero");
        int numero1=entrada.nextInt();
        System.out.println("ingresa el segundo numero");
        int numero2=entrada.nextInt();
        System.out.println("ingresa el tercer numero");
        int numero3=entrada.nextInt();
        
          if(numero1<numero2 && numero2<numero3)
            System.out.println( numero1+", "+numero2+", "+numero3);
        else{
            if(numero1<numero3 && numero3<numero2)
                System.out.println(numero1+", "+numero3+", "+numero2);
            else{
               if(numero2<numero1 && numero1<numero3)
                   System.out.println(numero2+", "+numero1+", "+numero3);
               else{
                  if(numero2<numero3&& numero3<numero1)
                      System.out.println(numero2+", "+numero3+", "+numero1);
                  else{
                      if(numero3<numero1 && numero1<numero2)
                         System.out.println(numero3+", "+numero1+", "+numero2);
                      else{
                         if(numero3<numero2 && numero2<numero1)
                            System.out.println(numero3+", "+numero2+", "+numero1);
                         
                        }
                      }
                   }
                }
            }
        }catch(Exception e){
            System.out.println(e);
            num();
        }
        }
    
    
//PEDIR UN NUMERO ENTRE EL CERO Y 10000 Y DECIR CUANTAS CIFRAS TIENE    
    
  public static void ejercicio2(){
   Scanner entrada = new Scanner(System.in);
      try{
            System.out.print("Introduce un número entero del 0 al 10000: ");
           int num = entrada.nextInt();
           if(num>0 || num<=10000){
               System.out.println("el numero ingresado no existe:");
           }
             int cifras= 0;    
            while(num !=0){             
                    num = num/10;         
                   cifras++;          
            }
            System.out.println("El número tiene " + cifras+ " cifras");                                           
      }catch(Exception e){
          System.out.println("introduce unicamente numeros"+e);
      }
     
           
          
    
      
  } 
  
  public static void notas(){
      //PEDIR UNA NOTA DEL CERO AL 10 Y MOSTRARLA DE LA FORMA: INSUFICIENTE SUFICIENTE BIEN NOTABLE SOBRE SALIENTE.
Scanner notas =new Scanner(System.in);
try{
        int nota;
        System.out.println("ingresa una nota del 0 al 10");
        nota=notas.nextInt();
       
        
              switch(nota){
                  case 0,1 , 2, 3:
                  
                      System.out.println("¡INSUFICIENTE!");
                      break;
                  case 4,5,6:
                 
                      System.out.println("¡SUFICIENTE!");
                      break;
                  case 7,8:
                      System.out.println("¡BIEN!");
                      break;
                  case 9:
                      System.out.println("¡NOTABLE!");
                      break;
                  case 10:
                      System.out.println("¡SOBRESALIENTE!");
                      break;
                  default:
                      System.out.println("¡no existe!!");
                      break;
              }          
              
              } catch(Exception e){
      System.out.println("ingresa numeros "+ e);
  }
  }


// pedir 10 numeros y mostrar la media de los numeros positivos la media de los numeros negativos y la cantidad de ceros
public static void ejemplo(){
Scanner entrada2 = new Scanner(System.in);
int positivos=0;
int negativos=0;
int ceros=0;
double mediap;
double mediaN;
int tp=0;
int tn=0;
for(int x=0; x<=9; x++){
    System.out.println("ingresa 10 numeros");
    int num=entrada2.nextInt();
if(num>0){
        tp++;
        positivos+=num;
        
        
    }else{
        if(num<0){
            tn++;
            negativos+=num;
        }else{
            ceros++;
        }
}  
    }
    mediap=(double)positivos/tp;
    mediaN=(double)negativos/tn;
    
    System.out.println("la media de positivos es: " +mediap);
    System.out.println("la media de negativos es: "+ mediaN);
    System.out.println("el total de ceros es: " + ceros);
    

    


        }
//pedir un numero del 0 al 99 y mostrarlo escrito por ejemplo para 56:cincuenta y  seis
public static void numero(){
	Scanner NUM= new Scanner(System.in);
        try{
	int num, unidad, decena;
    String unidades = "", masDecena = "", decenas = "";	
System.out.println("al ingresar 0 se finalizara el programa");
    System.out.println("Ingresa un numero del 0 aal 99: ");
    int nume;
    
    while ((nume=NUM.nextInt())>0){
       
        
     if(nume<0 || nume>99){
        System.out.println("el numero no valido:");
    }
   
  decena = nume / 10;
    unidad = (nume % 10) / 1;
   
    if (unidad == 1)
      unidades = "uno";
    else if (unidad == 2)
      unidades = "dos";
    else if (unidad == 3)
      unidades = "tres";
    else if (unidad == 4)
      unidades = "cuatro";
    else if (unidad == 5)
      unidades = "cinco";
    else if (unidad == 6)
      unidades = "seis";   
    else if (unidad == 7)
      unidades = "siete";   
    else if (unidad == 8)
      unidades = "ocho";
    else if (unidad == 9)
      unidades = "nueve";

    if (decena == 1 && unidad == 0)
      masDecena = "diez";
    else if (decena == 1 && unidad == 1)
     masDecena = "once";
    else if (decena == 1 && unidad == 2)
      masDecena = "doce";
    else if (decena == 1 && unidad == 3)
      masDecena= "trece";
    else if (decena == 1 && unidad == 4)
      masDecena = "catorce";
    else if (decena == 1 && unidad == 5)
      masDecena = "quince";
    else if (decena == 1 && unidad == 6)
      masDecena = "dieciseis";
    else if (decena == 1 && unidad == 7)
      masDecena = "diecisiete";
    else if (decena == 1 && unidad == 8)
      masDecena = "dieciocho";
    else if (decena == 1 && unidad == 9)
      masDecena = "diecinueve";   
   
    if (decena == 2)
      decenas = "veinte";
    else if(decena == 3)
      decenas = "treinta";
    else if(decena == 4)
      decenas = "cuarenta";
    else if(decena == 5)
      decenas = "cincuenta";
    else if(decena == 6)
      decenas = "sesenta";
    else if(decena == 7)
      decenas = "setenta";
    else if(decena == 8)
      decenas = "ochenta";
    else if(decena == 9)
      decenas = "noventa";   
   
    if (nume < 10)
      System.out.println("El numero es: "+ unidades);
    else if(nume < 20)
      System.out.println("El numero es: "+ masDecena);
    else if(nume < 100){
      if (nume % 10 == 0){
        System.out.println("El numero es: "+ decenas);
      }
      else{
        System.out.printf("El numero es: %s y %s\n",decenas,unidades);
      }
    }
   
  }
    
    System.out.println("fin ");
}catch(Exception e){
            System.out.println(e);
              numero();
}
      
}
//
}
