/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.main;

/**
 *
 * @author MVentura
 */
public class Empleados {
    private String nombre;
    private String apellido;
    private Integer edad;
    private String actividades_diarias;
    private String codigo_empleado;

    public Empleados(String nombre, String apellido, Integer edad, String codigo_empleado) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.codigo_empleado = codigo_empleado;
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getActividades_diarias() {
        return actividades_diarias;
    }

    public void setActividades_diarias(String actividades_diarias) {
        this.actividades_diarias = actividades_diarias;
    }

    public String getCodigo_empleado() {
        return codigo_empleado;
    }

    public void setCodigo_empleado(String codigo_empleado) {
        this.codigo_empleado = codigo_empleado;
    }
    
    
}
