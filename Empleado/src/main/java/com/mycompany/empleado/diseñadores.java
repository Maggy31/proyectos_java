/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.main;
import java.util.ArrayList;
import java.util.Collections;
public class diseñadores extends Empleados implements InterfazEmpleados{
    private String herramienta_diseño;
    private ArrayList<String> actividades_diarias=new ArrayList<>();

    public diseñadores(String herramienta_diseño, String nombre, String apellido, Integer edad, String codigo_empleado) {
        super(nombre, apellido, edad, codigo_empleado);
        this.herramienta_diseño = herramienta_diseño;
        
        setHerramienta_diseño("Photoshop");
        setCodigo_empleado("8000");
        
        actividades_diarias.add("reuniones de avance");
        actividades_diarias.add("elaboracion de diseño para paginas webs");
        actividades_diarias.add("presentacion-ajuste de diseño");
        
        if(edad <=25){
            actividades_diarias.add("revision de diseñador sr");
            
        }else{
            actividades_diarias.add("apoyo a diseñador jr");
        }
        
    }

    public void imprimir(){
    System.out.println("nombre: "+ getNombre() + "apellido: "+ getApellido()+ "edad: " + getEdad());
    }

    @Override
    public String toString() {
        return "diseñadores{" + "herramienta_diseño=" + getHerramienta_diseño() + ", actividades_diarias=" + getActividades_diarias() + '}';
    }
    
    
    
    public String getHerramienta_diseño() {
        return herramienta_diseño;
    }

    public void setHerramienta_diseño(String herramienta_diseño) {
        this.herramienta_diseño = herramienta_diseño;
    }
    
public void obtener_actividades(){
    Collections.sort(actividades_diarias);
    for(String diseño:actividades_diarias){
        System.out.println(diseño);
    }
    
}
    
}
