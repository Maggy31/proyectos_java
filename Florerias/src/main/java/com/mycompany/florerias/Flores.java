/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.florerias;


/**
 *
 * @author MVentura
 */
public class Flores {
    private String color;
    private String nombre;
    private double precio;
    private int cantidad;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Flores(String color, String nombre, double precio, int cantidad) {
        this.color = color;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public Flores() {
    }

    @Override
    public String toString() {
        return "Flores" + "\n"+ "color: " + color +"\n" +" nombre: " + nombre+ "\n"+ " precio: " + precio +"\n"+ "cantidad: " + cantidad +"\n";
    }
    
}

    
    