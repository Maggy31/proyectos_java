/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ejercicio_clinica;

/**
 *
 * @author MVentura
 */
public class Doctores {
    private static int codigo=1;
    private String nombreCompleto;
    private String domicilio;
    private String especialidad;
    private int id;

    public static int getCodigo() {
        return codigo;
    }

    public static void setCodigo(int codigo) {
        Doctores.codigo = codigo;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Doctores() {
    }

    public Doctores(String nombreCompleto, String domicilio, String especialidad, int id) {
        this.nombreCompleto = nombreCompleto;
        this.domicilio = domicilio;
        this.especialidad = especialidad;
        this.id = id;
        codigo++;
    }
   
    
    
    
   
    
    
}
