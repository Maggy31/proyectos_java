/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ejercicio_clinica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author MVentura
 */
public class Gestion {
    //crear una unidad
    //dar de alta paciente(unidad o clinica)
    //modificacion de paciente(cambiar valor de booleano)
    //Historial paciente (mostrar datos del paciente)
    //consulta info doctores
  
    //se crea un metodo estatico para no tener que crear un objeto para llamarlo a la main
    
    public static HashMap<Integer, Unidades>crearUnidad ( HashMap<Integer, Unidades>listaUnidades){
        Scanner dato=new Scanner(System.in);
        System.out.println("¿Cual es nombre de la nueva unidad?: ");
        String nombre = dato.nextLine();
        System.out.println("¿Cual es la seccion de la nueva unidad: ?");
        String seccion=dato.nextLine();
        Doctores doct=crearDoctor();
        HashMap < Integer, Pacientes>listaPaciente = new HashMap<>();
        listaUnidades.put(Unidades.getIdUnidad(), new Unidades(nombre,seccion, doct,listaPaciente));
        return listaUnidades;
    }
    
    //con este metodo tenemos los datos del doctor
    
    public static Doctores crearDoctor(){
       Scanner  dato=new Scanner(System.in);
        System.out.println("¿cual es el nombre del Doctor: ?");
        String nombre=dato.nextLine();
        System.out.println("¿Cual es el domicilio del Doctor?");
        String domicilio=dato.nextLine();
        System.out.println("¿Cual es la especialidad del Doctor?");
        String especialidad=dato.nextLine();
        Doctores doctor1=new Doctores(nombre,domicilio,especialidad, Doctores.getCodigo());
        System.out.println("  ");
        return doctor1;
        
       
    }
    
    public static HashMap<Integer, Unidades>crearPaciente ( HashMap<Integer, Unidades>listaUnidades){
     Scanner dato =new Scanner(System.in);
        System.out.println("¿Cual es el ID de la unidad ala que se quiere ingresar al paciente?");
        int idUnidad = dato.nextInt();
        dato.nextLine();
        System.out.println("¿Cual es el nombre del paciente?");
        String nombre =dato.nextLine();
        System.out.println("¿Cual es la edad del paciente?");
        String edad =dato.nextLine();
        System.out.println("¿Cual es el telefono del paciente?");
        String telefono = dato.nextLine();
        ArrayList<String> listaUnidadesPaciente=new ArrayList<>();
        listaUnidadesPaciente.add(listaUnidades.get( idUnidad).getNombre());
        //creamos otro hash map para almacenar los pacientes
        HashMap <Integer, Pacientes> listaPacientes=listaUnidades.get(idUnidad).getListaPacientes();
        Pacientes paciente = new Pacientes(nombre, edad,telefono,listaUnidadesPaciente,true);
        listaPacientes.put(Pacientes.getIDpaciente()-1, paciente);
//llamamos a la clase y hacemos un objeto para llamar la lista
       
       Unidades unidad = listaUnidades.get(idUnidad);
       // y aki es para cambiar el valor de la lista
       
       unidad.setListaPacientes(listaPacientes);
       listaUnidades.put(idUnidad, unidad);
        return listaUnidades; 
     
    }
    
    
    public static void menu(){
         Scanner dato=new Scanner(System.in);
        //creamos un hashmap vacio para llamarlo en cada metodo
        HashMap<Integer, Unidades> listaUnidades= new HashMap<>();
        //Clinica clinica=new Clinica();
       
        //crearDoctor();
       //crearUnidad(listaUnidades);
       //crearPaciente(listaUnidades);
       
        System.out.println("Clinica");
        System.out.println("Crear una unidad para empezar");
        crearUnidad(listaUnidades);
        boolean condicion;
        do{
         System.out.println("----------------------------\n"
                          +"|     que desea hacer        |\n"
                          +"-----------------------------|\n"
                          +"|    1 crear unidad          |\n"
                          +"|  2 crear un nuevo paciente |\n"
                          +"|     3 salir                |\n"            
                          +"|-----------------------------\n");
        int opcion =dato.nextInt();
        dato.nextLine();
       
        switch(opcion){
           
            case 1:
                crearUnidad(listaUnidades);
                condicion=true;
                break;
           
            case 2:
                crearPaciente(listaUnidades);
                condicion = true;
                break;
           
            case 3:
                condicion = false;
           break;
            default:
                System.out.println("Numero no valido");
                condicion = true;
        }
        }while(condicion);
        
    }
    

}
