/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ejercicio_clinica;

import java.util.ArrayList;



/**
 *
 * @author MVentura
 */
public class Pacientes {
    //creo atributos
    private static int IDpaciente = 1;
    private String nombre;
    private String edad;
    private String telefono;
   private ArrayList<String>listaUnidadesPaciente;
    private boolean enClinica;

    
    //creo set y get
    
    public static int getIDpaciente() {
        return IDpaciente;
    }

    public static void setIDpaciente(int IDpaciente) {
        Pacientes.IDpaciente = IDpaciente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public ArrayList<String> getListaUnidadesPaciente() {
        return listaUnidadesPaciente;
    }

    public void setListaUnidadesPaciente(ArrayList<String> listaUnidadesPaciente) {
        this.listaUnidadesPaciente = listaUnidadesPaciente;
    }

    public boolean isEnClinica() {
        return enClinica;
    }

    public void setEnClinica(boolean enClinica) {
        this.enClinica = enClinica;
    }

   //creo constructor vacio
    
    public Pacientes() {
    }

   //creo constructor con parametros
    public Pacientes(String nombre, String edad, String telefono, ArrayList<String> listaUnidadesPaciente, boolean enClinica) {
        this.nombre = nombre;
        this.edad = edad;
        this.telefono = telefono;
        this.listaUnidadesPaciente = listaUnidadesPaciente;
        this.enClinica = enClinica;
        IDpaciente++;
    }
    
    
    
}
