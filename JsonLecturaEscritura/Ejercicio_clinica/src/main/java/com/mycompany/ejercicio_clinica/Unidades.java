/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ejercicio_clinica;

import java.util.HashMap;


public class Unidades {
    //creo atributos
   private static int idUnidad= 1;
   private String nombre;
   private String seccion;
   private Doctores doctor;
   private HashMap <Integer, Pacientes> listaPacientes;
    
   
   
   
   //Creo set y get
   
   
   public static int getIdUnidad() {
        return idUnidad;
    }

    public static void setIdUnidad(int idUnidad) {
        Unidades.idUnidad = idUnidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public Doctores getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctores doctor) {
        this.doctor = doctor;
    }

    public HashMap<Integer, Pacientes> getListaPacientes() {
        return listaPacientes;
    }


    public void setListaPacientes(HashMap<Integer, Pacientes> listaPacientes) {
        this.listaPacientes = listaPacientes;
    }

//creo constructores
    public Unidades() {
    }

    public Unidades(String nombre, String seccion, Doctores doctor, HashMap<Integer, Pacientes> listaPacientes) {
        this.nombre = nombre;
        this.seccion = seccion;
        this.doctor = doctor;
        this.listaPacientes = listaPacientes;
        idUnidad++;
    }
}

   
    
    
   
    

