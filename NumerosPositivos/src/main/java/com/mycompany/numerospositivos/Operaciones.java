/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.numerospositivos;

/**
 *
 * @author MVentura
 */
public class Operaciones {
    private int numero;
    private int numero2;
    private int resultado;
    public Operaciones(int numero, int numero2, int resultado) {
        this.numero = numero;
        this.numero2 = numero2;
        this.resultado= resultado;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNumero2() {
        return numero2;
    }

    public void setNumero2(int numero2) {
        this.numero2 = numero2;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }
    

    public int suma(){
       this.resultado = this.numero + this.numero2; 
       return this.resultado;
    }
     public int resta (){
         this.resultado = this.numero - this.numero2;
         return this.resultado;
         
     }
      public int multiplicacion(){
          this.resultado = this.numero*this.numero2;
          return this.resultado;
      }
   
}  
         
    
            
    

