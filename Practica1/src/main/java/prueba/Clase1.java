/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prueba;

/**
 *
 * @author MVentura
 */
public class Clase1 {
    
    
    private String nombre;
    private String apellido;
    private Integer edad;
    private Integer telefono;
    private String direccion;
    //creamos los constructores
   
    
   
   
 public Clase1(String nombre,String apellido, Integer edad,  Integer telefono,String direccion){
    this.nombre=nombre;
    this.apellido=apellido;
    this.edad=edad;
    this.telefono=telefono;
    this.direccion=direccion;
     }
    //setter y getter de los atributos//
    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombre){
            this.nombre=nombre;
}
    public String getApellido(){
        return apellido;
    }
    public void setApellido(String apellido){
            this.apellido=apellido;
    }
     public int getEdad(){
        return edad;
    }
    public void setEdad(int edad){
            this.edad=edad;
    }
    public int getTelefono(){
        return telefono;
    }
    public void setTelefono(int telefono){
            this.telefono=telefono;
    }
   
    public String getDireccion(){
        return direccion;
    }
    public void setDireccion(String direccion){
            this.direccion=direccion;
   
}
}

