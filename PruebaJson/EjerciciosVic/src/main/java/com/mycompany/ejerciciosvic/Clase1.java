/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ejerciciosvic;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author MVentura
 */
public class Clase1 {
    public static void num(){
        //PEDIR AL USUARIO TRES NUMEROS Y LOS MUESTRE ORDENADOS DE MENOR A MAYOR
        Scanner entrada = new Scanner (System.in);
        try{
        System.out.println("ingresa el primer  numero");
        int numero1=entrada.nextInt();
        System.out.println("ingresa el segundo numero");
        int numero2=entrada.nextInt();
        System.out.println("ingresa el tercer numero");
        int numero3=entrada.nextInt();
        
          if(numero1<numero2 && numero2<numero3)
            System.out.println( numero1+", "+numero2+", "+numero3);
        else{
            if(numero1<numero3 && numero3<numero2)
                System.out.println(numero1+", "+numero3+", "+numero2);
            else{
               if(numero2<numero1 && numero1<numero3)
                   System.out.println(numero2+", "+numero1+", "+numero3);
               else{
                  if(numero2<numero3&& numero3<numero1)
                      System.out.println(numero2+", "+numero3+", "+numero1);
                  else{
                      if(numero3<numero1 && numero1<numero2)
                         System.out.println(numero3+", "+numero1+", "+numero2);
                      else{
                         if(numero3<numero2 && numero2<numero1)
                            System.out.println(numero3+", "+numero2+", "+numero1);
                         
                        }
                      }
                   }
                }
            }
        }catch(Exception e){
            System.out.println(e);
            num();
        }
        }
    
    
//PEDIR UN NUMERO ENTRE EL CERO Y 10000 Y DECIR CUANTAS CIFRAS TIENE    
    
  public static void ejercicio2(){
   Scanner entrada = new Scanner(System.in);
      try{
            System.out.print("Introduce un número entero del 0 al 10000: ");
           int num = entrada.nextInt();
             int cifras= 0;    
            while(num !=0){             
                    num = num/10;         
                   cifras++;          
            }
            System.out.println("El número tiene " + cifras+ " cifras");                                           
      }catch(Exception e){
          System.out.println("introduce unicamente numeros"+e);
      }
     
           
          
    
      
  } 
  
  public static void notas(){
      //PEDIR UNA NOTA DEL CERO AL 10 Y MOSTRARLA DE LA FORMA: INSUFICIENTE SUFICIENTE BIEN NOTABLE SOBRE SALIENTE.
Scanner notas =new Scanner(System.in);
try{
        int nota;
        System.out.println("ingresa una nota del 0 al 10");
        nota=notas.nextInt();
              switch(nota){
                  case 1 , 2, 3:
                  
                      System.out.println("¡INSUFICIENTE!");
                      break;
                  case 4,5,6:
                 
                      System.out.println("¡SUFICIENTE!");
                      break;
                  case 7,8:
                      System.out.println("¡BIEN!");
                      break;
                  case 9:
                      System.out.println("¡NOTABLE!");
                      break;
                  case 10:
                      System.out.println("¡SOBRESALIENTE!");
                      break;
                  default:
                      System.out.println("¡no existe!!");
                      break;
              }          
              
              } catch(Exception e){
      System.out.println("ingresa numeros "+ e);
  }
  }
}

