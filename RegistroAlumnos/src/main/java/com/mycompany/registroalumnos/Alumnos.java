/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.registroalumnos;

/**
 *
 * @author MVentura
 */
public class Alumnos {
    private String nombre;
    private String apellido;
    private double calificacion;
    private int edad;

    public Alumnos(String nombre, String apellido, double calificacion, int edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.calificacion = calificacion;
        this.edad = edad;
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "Alumnos"+"\n" + "nombre: " + nombre +"\n"+ " apellido:  " + apellido+"\n" + " calificacion:" + calificacion+"\n" + " edad: " + edad + "\n";
    }
   
    
}
