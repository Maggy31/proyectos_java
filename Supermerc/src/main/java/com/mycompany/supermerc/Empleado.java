/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.supermerc;

import java.util.ArrayList;
import java.util.List;


public class Empleado {
    private String nombreCompleto;
    private int edad;
    private int Antiguedad;
    private String tipo=null;
    private List<String> nombre =new ArrayList<Empleado>(); 
    
    public Empleado(){
        
    }
    public Empleado(String nombreCompleto, int edad, int Antiguedad, String tipo, ArrayList<String> nombre) {
        this.nombreCompleto = nombreCompleto;
        this.edad = edad;
        this.Antiguedad = Antiguedad;
        this.tipo=tipo;
        this.nombre=nombre;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getAntiguedad() {
        return Antiguedad;
    }

    public void setAntiguedad(int Antiguedad) {
        this.Antiguedad = Antiguedad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<String> getNombre() {
        return nombre;
    }

    public void setNombre(List<String> nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Empleado{" + "nombreCompleto=" + nombreCompleto + ", edad=" + edad + ", Antiguedad=" + Antiguedad + ", tipo=" + tipo + ", nombre=" + nombre + '}';
    }
    

    
          
     }


    
}
