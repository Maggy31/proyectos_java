/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.tallermecanic;

import java.util.List;
import java.util.Scanner;


public class Auto {
    private String serie;
    private String modelo;
    private String placas;
    private String color;
    private int ID_auto;
 private static int au=1;
 
 
    public Auto(String serie, String modelo, String placas, String color, int ID_auto) {
        this.serie = serie;
        this.modelo = modelo;
        this.placas = placas;
        this.color = color;
        this.ID_auto=ID_auto;
        Auto.au ++;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getID_auto() {
        return ID_auto;
    }

    public void setID_auto(int ID_auto) {
        this.ID_auto = ID_auto;
    }

    public static int getAu() {
        return au;
    }

    public static void setAu(int au) {
        Auto.au = au;
    }

    public Auto() {
    }
    

    @Override
    public String toString() {
        return "-----------------------------\n"
              +"|          Auto               \n"
              +"------------------------------\n"
              +"|         serie=" + serie +  "|\n"
              +"|         modelo=" + modelo +"|\n"
              +"|         placas=" + placas + "\n"
              +"|         color=" + color + "\n"
              +"|        ID_auto=" + ID_auto +"\n"
               +"-------------------------------\n";
    }
    
    
    
    
    
   public void altaAuto(List<Auto>autoss){
          Scanner aut= new Scanner(System.in);
       
    
        
    System.out.println("Ingresa el numero de serie : ");
          String numSerie=aut.nextLine();
          
       
        System.out.println("Ingresa el modelo:  ");
         String mode=aut.nextLine();
         
          System.out.println("Ingresa las placas :  ");
         String placa=aut.nextLine();
        
            System.out.println("ingresa el color: ");
                String  colo=aut.nextLine();
                
        
     
      Cliente cliente2;
        autoss.add(new Auto(numSerie,mode,placa,colo, au));
        
       
   }
      
   public void bajaAuto(List<Auto>autoss){
         Scanner datos=new Scanner(System.in);
        System.out.println("elige el auto para eliminar:");
        int elimina=datos.nextInt();
        
        System.out.println("eliminar auto:"+ elimina);
           autoss.remove(elimina -1);
           
           System.out.println(autoss.toString());
    }
    
     
       
       
       
       
   
   public void consultaAuto(List<Auto>autoss){
        Scanner entrada = new Scanner(System.in);
        System.out.println("elige un auto para consultar: ");
        int id=entrada.nextInt();
       System.out.println( autoss.get(id-1).ID_auto);
         System.out.println("nombre del cliente "+autoss.get(id-1));
       
    } 
   
   public void editar(List<Auto>autoss){
        Scanner auto1=new Scanner (System.in); 
        System.out.println("elige al numero de auto que deseas editar: ");
        int num=auto1.nextInt();
       System.out.println("-----------------------------\n"
                         +"|      Que deseas editar:    \n"
                         +"-----------------------------\n"
                         +"|  1         numero de serie  \n"
                         +"|  2    modelo                \n"
                         +"|  3   placas                \n"
                         +"|  4    color                \n"
                         +"|----------------------------\n");
        
      int x= auto1.nextInt();
      auto1.nextLine();
      
       switch (x){
           case 1:
               System.out.println("ingresa el numero de serie a modificar");
               String ser=auto1.nextLine();
               autoss.get(num-1).serie=ser;
               
               System.out.println("El numero de serie actual es: "+ autoss.get(num-1).serie);
                System.out.println(autoss.toString());
               editar(autoss);
       
               break;
           case 2:
               System.out.println("ingresa el modelo a modificar");
               String mode =auto1.nextLine();
               autoss.get(num -1).modelo=mode;
               System.out.println("El modelo actual es: " + autoss.get(num -1).modelo);
               System.out.println(autoss.toString());
               editar(autoss);
               break;
               
           case 3: 
               System.out.println("Ingresa las placas a modificar: ");
               String pla =auto1.nextLine();
               autoss.get(num - 1).placas=pla;
               System.out.println("la placa  actual es: "+ autoss.get(num -1).placas);
               System.out.println(autoss.toString());
           editar(autoss);
           break;
           
       }}
           
              public void menu(List<Auto>autoss){
        
        System.out.print("-------------------------  \n"
                        +"|  QUE DESEAS REALIZAR:   |\n" 
                        +"------------------------- |\n"
                        +"|  1. Alta de auto     |\n"
                        +"|  2.Eliminar auto     |\n"
                        +"|  3.Consultar auto    |\n"
                        +"|  4.editar auto       |\n"
                        +"|  5. salir               |\n"
                        +"|-------------------------|\n"
                        +"    Elije una opcion    \n");
        Scanner menu2=new Scanner(System.in);
        try{
        int auto=menu2.nextInt();
        menu2.nextLine();
        
        switch (auto){
            case 1:
                   System.out.println("ingresa el  numero de serie");
                String auto1=menu2.nextLine();
                System.out.println("ingresa el modelo");
                String modelo1=menu2.nextLine();
                 System.out.println("ingresa las placas");
                String placas1=menu2.nextLine();
                 System.out.println("ingresa el color");
                String  color1=menu2.nextLine();
                
                autoss.add(new Auto(auto1,modelo1,placas1,color1,au));
                 for(Auto menu : autoss){
                    System.out.println(menu);
                }
                menu(autoss);
                break;
               
            case 2:
               bajaAuto(autoss);
                menu( autoss);
                break;
               
            case 3:
                
                
        System.out.println("elige un auto para consultar: ");
        int id=menu2.nextInt();
       System.out.println( autoss.get(id-1).ID_auto);
         System.out.println("auto"+autoss.get(id-1));
       
                
                menu(autoss);
                break;
              
                
            case 4: 
               editar(autoss);
               
                 
                menu(autoss);
                break;
               
               
                 
            
          
               
            case 5:
                
                System.out.println("salir");
                break;
        }
        }catch(java.util.InputMismatchException e){
                System.out.println("error: valor no valido"+e);
           menu(autoss);     
        }
        
        
         
}
    }


           
       
   


   
       
   
    

