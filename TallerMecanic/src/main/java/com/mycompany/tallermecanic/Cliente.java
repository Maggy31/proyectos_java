/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.tallermecanic;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Cliente {
    private String nombre;
    private String apellidoP;
    private String domicilio;
    private List<Auto>autoss=new ArrayList<Auto>();
    private int IDcliente;
  
   private static int cli=1;
    
   
   public Cliente(String nombre, String apellidoP, String domicilio, List<Auto>autoss, int IDcliente) {
        this.nombre = nombre;
        this.apellidoP = apellidoP;
        this.domicilio = domicilio;
        this.autoss = autoss;
        this.IDcliente=IDcliente;
        cli++;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public List<Auto> getAuto() {
        return autoss;
    }

    public void setAuto(Auto[] auto) {
        this.autoss = autoss;
    }

    public int getIDcliente() {
        return IDcliente;
    }

    public void setIDcliente(int IDcliente) {
        this.IDcliente = IDcliente;
    }

    public Cliente() {
        
    }

    @Override
    public String toString() {
        return  "--------------------------------------\n"
               +"|       CLIENTES                       \n"
               +"|--------------------------------------\n"
               +"|nombre: "+this.nombre+               "\n"
               +"|Apellido Paterno: "+ this.apellidoP+ "\n"
               +"|Domicilio: "+this.domicilio +        "\n"
               +"|Auto: "+autoss +                  "\n"
               +"|ID Cliente:"+this.IDcliente+         "\n"
               +"|--------------------------------------\n";
       
                
    }
    
    
     public void altaCliente(List<Cliente>cliente, List<Auto>autoss){
        Auto aut=new Auto();
        Scanner datos1= new Scanner(System.in);
        System.out.println("Ingresa el nombre del cliente : ");
          String nombre1=datos1.nextLine();
         System.out.println("Ingresa el apellido paterno:  ");
         String apellido=datos1.nextLine();
         System.out.println("Ingresa el  domicilio :  ");
         String domicilio1=datos1.nextLine();
         cliente.add(new Cliente(nombre1,apellido,domicilio1,autoss,cli));
       aut.menu(autoss);
       
         System.out.println(cliente);
         menu(cliente);
         
	 }
    
  
    
    public void baja(List<Cliente>cliente){
          Scanner datos2=new Scanner(System.in);
        System.out.println("elige un cliente para eliminar:");
        int elimina=datos2.nextInt();
        
        System.out.println("eliminar cliente:"+ elimina);
           cliente.remove(elimina -1);
           
           System.out.println(cliente.toString());
    }
    
    public void consulta( List<Cliente>cliente){
       Scanner datos3 = new Scanner(System.in);
        System.out.println("elige un cliente para consultar: ");
        int id=datos3.nextInt();
       System.out.println( cliente.get(id-1).IDcliente);
         System.out.println("nombre del cliente "+cliente.get(id-1));
       
    }  
    
    public void modificar(List<Cliente>cliente){
         Scanner datos4=new Scanner (System.in); 
        System.out.println("elige al numero de cliente que deseas editar: ");
        int nomb=datos4.nextInt();
       System.out.println("-----------------------------\n"
                         +"|      Que deseas editar:    \n"
                         +"-----------------------------\n"
                         +"|  1         Nombre          \n"
                         +"|  2    Apellido paterno     \n"
                         +"|  3   Domicilio             \n"
                         +"|  4     Auto                \n"
                         +"|  6      SALIR              \n"
                         +"|----------------------------\n");
        
      int dat= datos4.nextInt();
      datos4.nextLine();
      
       switch (dat){
           case 1:
               System.out.println("ingresa el nombre a modificar");
               String nueNom=datos4.nextLine();
               cliente.get(nomb-1).nombre=nueNom;
               
               System.out.println("El actual nombre es: "+ cliente.get(nomb-1).nombre);
                System.out.println(cliente.toString());
               modificar(cliente);
       
               break;
           case 2:
               System.out.println("ingresa el apellido a modificar");
               String ape =datos4.nextLine();
               cliente.get(nomb -1).apellidoP=ape;
               System.out.println("El actual apellido es: " + cliente.get(nomb -1).apellidoP);
               System.out.println(cliente.toString());
               modificar(cliente);
               break;
               
           case 3: 
               System.out.println("Ingresa el domicilio a modificar: ");
               String dom =datos4.nextLine();
               cliente.get(nomb - 1).domicilio=dom;
               System.out.println("El domicilio actual es: "+ cliente.get(nomb -1).domicilio);
               System.out.println(cliente.toString());
           modificar(cliente);
           break;
           case 4: 
           
       }
        
        
       }
    
        public void menu(List<Cliente>cliente){
        
        System.out.print("-------------------------  \n"
                        +"|  QUE DESEAS REALIZAR:   |\n" 
                        +"------------------------- |\n"
                        +"|  1. Alta de cliente     |\n"
                        +"|  2.Eliminar cliente     |\n"
                        +"|  3.Consultar cliente    |\n"
                        +"|  4.editar cliente       |\n"
                        +"|  5. salir               |\n"
                        +"|-------------------------|\n"
                        +"    Elije una opcion    \n");
        Scanner menu=new Scanner(System.in);
        try{
        int x =menu.nextInt();
        menu.nextLine();
        
        switch (x){
            case 1:
                List<Auto> autoss = new ArrayList<Auto>();
                  
              altaCliente(cliente, autoss);
                break;
               
            case 2:
               baja(cliente);
                menu( cliente);
                break;
               
            case 3:
                
                
        System.out.println("elige un cliente para consultar: ");
        int id=menu.nextInt();
       System.out.println( cliente.get(id-1).IDcliente);
         System.out.println("Datos del cliente: "+cliente.get(id-1));
       
                
                menu(cliente);
                break;
              
                
            case 4: 
               modificar(cliente);
               
                 
                menu(cliente);
                break;
               
               
                 
            
          
               
            case 5:
                
                System.out.println("salir");
                break;
        }
        }catch(java.util.InputMismatchException e){
                System.out.println("error: valor no valido"+e);
           menu(cliente);     
        }
        
        
         
}
    }


    
    
    
    

    

  
    

   