/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.traductor;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Traductor {
    
    public static void main(String[] args) {
    
    System.out.println("Traductor de Español-frances ");
    Scanner tr= new Scanner(System.in);
    System.out.println("Introduce palabra en español: ");
    Map<String, String> traductor = new HashMap<String, String>();
    traductor.put("hola", "salut");
    traductor.put("amiga", "amie");
    traductor.put("rosa", "rose");
    traductor.put("ayuda", "aider");
    traductor.put ("adios","adieu");

    Map<String, String> y = new HashMap<>();
    for(String key : traductor.keySet()){
        y.put(traductor.get(key), key);
    }

    String texto = tr.next().toLowerCase().trim();
    tr.close();

    StringBuffer cadena = new StringBuffer();
    for(String frase : texto.split(" ")){
        String buscar = traductor.get(texto);

        if (buscar == null){
            buscar = texto;
        }

        if(cadena.length() != 0){
            cadena.append(" ");
        }

        cadena.append(buscar);
    }
    System.out.println(cadena.toString());
    }
        }

