/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import 
public class Triangulo extends Figura{
    
    public Triangulo(int ancho, int alto,int cordenada1, int cordenada2, double area, double perimetro){
        super(ancho,alto,perimetro,area, cordenada1, cordenada2);
    }
        @Override
         public double area1(){
        
             int resultado= (this.getAncho() * this.getAlto())/2;
             return resultado;
        
    }
    
}
