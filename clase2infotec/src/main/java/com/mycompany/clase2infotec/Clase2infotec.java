/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.clase2infotec;

/**
 *
 * @author MVentura
 */
public class Clase2infotec {

    public static void main(String[] args) {
       int entero1=10;
       int entero2;
       System.out.println("valor de entero es: " + entero1);
       entero2= 3+3;
       System.out.println("el valor de entero es: "+ entero2);
       
       String palabra= "hola";
       System.out.println(palabra.toLowerCase());
       System.out.println(palabra.toUpperCase());
      
       
     Persona persona1=new Persona("maggi",28);
     System.out.println("nombre: "+persona1.nombre + "\n"+ "edad: "+persona1.edad);
      System.out.println(persona1.getClass().getSimpleName());
        System.out.println(palabra.getClass().getSimpleName());
        
        
      var varEntero = 80;
      System.out.println("entero: " + varEntero);
      System.out.println(((Object)varEntero).getClass().getSimpleName());
      
      var varCadena1 = "hola mundo";
      System.out.println("cadena" + varCadena1);
       System.out.println(((Object)varCadena1).getClass().getSimpleName());
       
       var varBolean1=false;
       System.out.println("BOLEAN" + varBolean1);
       System.out.println(((Object)varBolean1).getClass().getSimpleName());
       
       var $hola="miguel";
       int _hola2=24;
       //int #hola3=21;
      int én=33;
      //una constante se difine con la palabra reservada final y no se puede modificar
       
      final int uno=23;
      System.out.println(uno);
    }
}
class Persona{
    public String nombre;
    public Integer edad;

    public Persona(String nombre, Integer edad) {
        this.nombre = nombre;
        this.edad=edad;
       
    }
    
}