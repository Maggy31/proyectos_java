/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.clasecristina;

/**
 *
 * @author MVentura
 */
public class Topper {
    public static int contador;
    private String color;
    private Integer capacidad;
    private String tamaño;
    private String caracteristicas;
    
    public Topper(String color){
        
    }

    public Topper(String color, Integer capacidad, String tamaño, String caracteristicas) {
        this.color = color;
        this.capacidad = capacidad;
        this.tamaño = tamaño;
        this.caracteristicas = caracteristicas;
        Topper.contador++;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }
    public Topper(){
        
    }
    public String datos(){
        return "Topper: " + "color: "+ color+"capacidad: " +capacidad+ "tamaño: "+ tamaño+"caractereisticas"+ caracteristicas;
    }
    public static void contaTopper(){
        System.out.println("total de topper: "+Topper.contador);
    }

    @Override
    public String toString() {
        return "Topper{" + "color=" + color + ", capacidad=" + capacidad + ", tama\u00f1o=" + tamaño + ", caracteristicas=" + caracteristicas + '}';
    }
    
}

    

