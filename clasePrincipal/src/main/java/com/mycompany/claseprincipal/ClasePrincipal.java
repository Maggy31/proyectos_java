/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.claseprincipal;

/**
 *
 * @author MVentura
 */
public class ClasePrincipal {

    public static void main(String[] args) {
         

        //Datos primitivos.
        int entero1 = 10;
        int entero2;


        System.out.println("Hola mundo" );

        System.out.println("El valor del entero 1 es: " + entero1);

        //Los enteros no inicializados tienen valor 0.
        entero2 = 5 + 5;

        System.out.println("El valor del entero 2 es: " + entero2 );

        //Datos tipo objeto o referencia.
        String palabra = "Hola";
        System.out.println(palabra.toLowerCase());
        System.out.println(palabra.toUpperCase() + "\n");

        //Los datos de tipo string ya son parte de una clase por defecto.
        String palabra2 = new String(" mundo!");
        System.out.println(palabra + palabra2 + "\n");

        Persona p1Persona = new Persona("Marcos", 22);
        System.out.println("Nombre: " + p1Persona.nombre);
        System.out.println("Edad: " + p1Persona.edad);

        //Todos los tipos de datos no inicializados empiezan con valor null.
        Persona p2Persona = new Persona();
        p2Persona.nombre = "Aarón";
        System.out.println("Nombre: " + p2Persona.nombre);
        System.out.println("Edad: " + p2Persona.edad );

        //Se imprime el tipo de dato al que pertenece el on¿bjeto o la clase.
        System.out.println(p2Persona.getClass().getSimpleName());
        System.out.println(palabra.getClass().getSimpleName());

        //Los datos primitivos no son objetos y no cuentan con metodos.
         //El ejemplo de abajo marca error al descomentarlo.
        //System.out.println(entero1.getClass.getSimpleName);

        //Primero se debe convertir el dato primitivo en objeto para poder utilizar metodos sobre el.
        System.out.println(((Object)entero1).getClass().getSimpleName());
        
        var varEntero = 80;
        System.out.println("varEntero:  " + varEntero);
        System.out.println(((Object)varEntero).getClass().getSimpleName());

        var varcadena1 = "Hello word";
        System.out.println("varcadena1:  " + varcadena1);
        System.out.println(((Object)varcadena1).getClass().getSimpleName());

        var varBollean1 = false;
        System.out.println("varBollean1:  " + varBollean1);
        System.out.println(((Object)varBollean1).getClass().getSimpleName());
        
        //var varEntero = 8.5;

        //var tito;//No esta inicializado
        //var $hola = "Miguel";
        //int _hola2 = 45;
        //int #hola3 = 96;
        //int íla  = 8;
        //int ñoño = 8;
        
        final int luisa = 90;//Declarar constante
        System.out.println("luis:  " + luisa);
        //luisa = 8;
    }
}
//Se crea clase Persona.
class Persona{
    public String nombre;
    public int edad;

    //Metodo constructor de la clase Persona con sus respectivos parametros.
    public Persona(String nombre, Integer edad){
        this.nombre = nombre;
        this.edad = edad;
    }

    //Se declara un metodo constructor vacio
    public Persona(){

    }
}
    

