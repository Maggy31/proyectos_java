/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.complementaria19y20;

/**
 *
 * @author MVentura
 */
public class Celular extends Computadora{
    public Integer año;
    public Integer numeroCamaras;

    public Celular(String Marca, String numeroDeSerie, String RAM, Integer año, Integer numeroCamaras) {
        super(Marca, numeroDeSerie, RAM);
        this.año= año;
        this.numeroCamaras=numeroCamaras;
         Computadora.contador++;
    }
    public void leerInfo2(){
        super.leerInfo();
        System.out.println("El año es: " + this.año + "\n"+"Numero de camaras es :" + this.numeroCamaras);
    }
    
}
