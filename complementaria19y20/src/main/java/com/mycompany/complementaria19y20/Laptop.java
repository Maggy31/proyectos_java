/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.complementaria19y20;

/**
 *
 * @author MVentura
 */
public class Laptop extends Computadora {
    String color;
    Integer año;
    Integer precio;

    public Laptop(String color, Integer año, Integer precio, String Marca, String numeroDeSerie, String RAM) {
        super(Marca, numeroDeSerie, RAM);
        this.color = color;
        this.año = año;
        this.precio = precio;
         Computadora.contador++;
    }
    
    public  void lap1(){
        super.leerInfo();
        System.out.println("el color es : " + this.color+ "\n"+ "El año es :" + this.año +"\n" + " el precio es: " + this.precio);
    }
    
    
    
}
