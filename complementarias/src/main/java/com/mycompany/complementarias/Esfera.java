/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.complementarias;


 /**
Ejer 3: Crear un programa que Calcule el área, volumen y la longitud de la 
* circunferencia de una esfera con el mismo radio.   
 
 * 
 * area=4(3.1416)radio*radio
 *volumen=(4(3.1416)radio*radio*radio)/3
 * circunferencia=2(3.1416)*radio
 */
 
public class Esfera {
    private double area;
    private double volumen;
    private double radio;
    private double pi=3.1416;
    private double circunferencia;

    public Esfera(double area, double volumen, double radio, double circunferencia) {
        this.area = area;
        this.volumen = volumen;
        this.radio = radio;
        this.circunferencia = circunferencia;
    }
    
    public Esfera(double radio){
        this.radio=radio;
    }

    
    public void calcularArea(){
      this.area=(4*pi)*this.radio*this.radio;
      System.out.println("area = " + this.area);
        
    }
    public double calcularVolumen(){
        this.volumen=(4*pi)*(this.radio*this.radio*this.radio);
        return this.volumen/3;
    }
     public void calcularCircunferencia(){
      this.circunferencia=(2*pi)*this.radio;
      System.out.println("circunferencia = " + this.circunferencia);
        
    }
   /* public static void calcularCircunferencia(double radio1){
       double circunferencia1;
        double pi2=3.1416;
        
        circunferencia1=(2*pi2)*radio1;
        System.out.println("circunferencia = " + circunferencia1);
        
    }
   */
    
}
