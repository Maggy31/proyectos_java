/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.ejemplodocumento;

/**
 *
 * @author MVentura
 */
public class EjemploDocumento {

    public static void main(String[] args) {
        //nos permite ejecutar una o varias lineas de codigo de forma iterativa o repetitiva 
        for(int x=0; x<=10; x=x+2){
            
         System.out.println( x);
        
        }
        System.out.println(" fin del ciclo for "+"\n");
        int control=20;
        //el while evalua la condicion y al no cumplirse no se ejecuta
        while(control >0){
            System.out.println("dentro del ciclo" + control);
            control=control-2;
        }
        System.out.println("FIN DEL CICLO while ");
        
        
        //el do while nos permite ejecutar por lo menos una vez la sentencia ya que la codicion se evalua despues
        int opcion;
        do{
             System.out.println("elije una opcion");
             opcion=10;
              System.out.println("elijio la opcion: "+opcion);
        }while(opcion !=10);
        
        
    }
}
