/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.estructura;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
public class Estructura {

   public static void main(String[] args) {
        Map <Integer,String> mapa1 = Map.of(1, "Luis", 2, "Ernesto",
        3, "Fernando");
        System.out.println("Elemento [3] de mapa1: " + mapa1.get(3));

        System.out.println("\nImpresion elementos mapa1:");
        for (Map.Entry < Integer, String > entrada: mapa1.entrySet()){
            System.out.println(entrada.getKey()+" ->"+ entrada.getValue());
        }
        //No se puede mandar a llamar directamente al map sino mandar a traer una 
        //clase que implementa el map.
        System.out.println("\nEjemplo TreeMap: \n");
        Map <Integer,String> mapa2 = new TreeMap<>();
        mapa2.put(1, "Luis");
        mapa2.put(2, "Ernesto");
        mapa2.put(3, "Fernando");
        mapa2.put(3, "Ana");
        mapa2.put(4, "Ana");

        for (Map.Entry < Integer, String > entrada: mapa2.entrySet()){
            System.out.println(entrada.getKey()+" ->"+ entrada.getValue());
        }
        System.out.println("\nEjemplo HashMap: \n");
        Map <Integer,String> map = new HashMap<Integer, String>();
        map.put(17, "Yerenia");     map.put(7, "Mariana");
        map.put(16, "Rodrigo");     map.put(8, "Sahile");
        map.put(3, "Emmanuel");     map.put(5, "Liz");
        map.put(11, "Miriam");      map.put(14, "Damian");
        map.put(18, "Sahile");      map.put(6, "Dafne");
        map.put(1, "Carlos");       map.put(15, "Claudia");

        //Imprimimos el Map con un Iterador
        Iterator<Integer> it = map.keySet().iterator();
        while(it.hasNext()){
        Integer key = it .next();
        System.out.println("Numero alumno: " + key + " -> Nombre: " + map.get(key));
        }
    }
}


