/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.main;

/**
 *
 * @author MVentura
 */
public class Empleados  {
  private static int id=1;
  private int idEmpleado;
    private String nombre;
    private String apellido;
     private String tipoEmpleado;
    private Integer edad;
    
    private String codigo_empleado;

    public Empleados() {
    }

    public Empleados(int idEmpleado,  String nombre, String apellido, String tipoEmpleado, Integer edad, String codigo_empleado) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipoEmpleado = tipoEmpleado;
        this.edad = edad;
        this.codigo_empleado = codigo_empleado;
        this.idEmpleado = id;
        id++;
    }

  
    
    
    public Empleados(String nombre, String apellido, Integer edad, String codigo_empleado) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.codigo_empleado = codigo_empleado;
        this.tipoEmpleado=tipoEmpleado;
   
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

   

    public String getCodigo_empleado() {
        return codigo_empleado;
    }

    public void setCodigo_empleado(String codigo_empleado) {
        this.codigo_empleado = codigo_empleado;
    }

    public String getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(String tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Empleados.id = id;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    
    
    
}
