/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.main;

import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author MVentura
 */
public class Gestion {
    public static HashMap<Integer, Empleados> llenarLista(HashMap<Integer, Empleados> listaEmpleados){
      listaEmpleados.put(Empleados.getId(), new Programadores(Empleados.getId(), "Leonardo", "Salgado Lara", "Programador", 20, null, null));
        listaEmpleados.put(Empleados.getId(), new Programadores(Empleados.getId(), "Fabian", "Martinez Salgado", "Programador", 30, null, null));
       listaEmpleados.put(Empleados.getId(), new Programadores(Empleados.getId(), "Ernesto", "villa Martinez", "Programador", 40, null, null));
        listaEmpleados.put(Empleados.getId(), new diseñadores(Empleados.getId(),"Miranda", "Sevilla Rico", "Diseñador", 20, null, null));
       listaEmpleados.put(Empleados.getId(), new diseñadores(Empleados.getId(),"Laura", "Salinas Mora", "Diseñador", 25, null, null));
       listaEmpleados.put(Empleados.getId(), new diseñadores(Empleados.getId(),"Liliana", "Lara Garcia", "Diseñador", 30, null, null));
       listaEmpleados.put(Empleados.getId(), new diseñadores(Empleados.getId(),"Pedro", "Camacho Linares", "Diseñador", 42, null, null));
      
       
       return listaEmpleados; 
   
      
  }
        
        
        
        
        
    
  
  
  public static void  consultarEmpleado(HashMap<Integer, Empleados> listaEmpleados){
      Scanner datos2 = new Scanner(System.in);
       System.out.println("quires  consultar todos los empleados o solo uno:\n 1. todos\n2.uno  ");
       int empleado2=datos2.nextInt();
       datos2.nextLine();
       if(empleado2==1){
           llenarLista(listaEmpleados);
           System.out.println(listaEmpleados);
          
           
       }else if(empleado2==2){
           System.out.println("que tipo de empleado quieres consultar\n 1.diseñadores\n2.programadores");
           int consultar=datos2.nextInt();
           datos2.nextLine();
           if(consultar==1){
               System.out.println("elige un diseñador");
               int diseñador=datos2.nextInt();
               
               System.out.println(listaEmpleados.get(diseñador));
               
           }else if(consultar==2){
               System.out.println("elige un programador");
               int pro=datos2.nextInt();
               
                       
               System.out.println(listaEmpleados.get(pro));
           }
       }
   
       
  
  }
   public static HashMap<Integer, Empleados> agregarEmpleado(HashMap<Integer, Empleados> listaEmpleados){
           Scanner datos2 = new Scanner(System.in);
          System.out.println("que tipo de empleado quieres agregar\n 1.diseñadores\n2.programadores");
           int ingresar=datos2.nextInt();
           datos2.nextLine();
           if(ingresar==1){
               System.out.println("agrega un diseñador");
               System.out.println("ingresa el nombre");
               String nom=datos2.nextLine();
               System.out.println("ingresa apellidos");
               String ape=datos2.nextLine();
               System.out.println("ingresa la edad");
               int edad=datos2.nextInt();
               
               listaEmpleados.put(diseñadores.getId(),new diseñadores(diseñadores.getId(), nom, ape, ape, edad, null, null));
            
               
           }else if(ingresar==2){
               System.out.println("agrega  un programador");
               System.out.println("ingresa el nombre");
               String nom=datos2.nextLine();
               System.out.println("ingresa apellidos");
               String ape=datos2.nextLine();
               System.out.println("ingresa la edad");
               int edad=datos2.nextInt();
               
           
               listaEmpleados.put(Programadores.getId(), new Programadores(Programadores.getId(), nom, ape, ape, edad, null, null));
           }
               
           
           
           return listaEmpleados;
           
           
     
           
           } 
   public static HashMap<Integer, Empleados> eliminarEmpleado(HashMap<Integer, Empleados> listaEmpleados){
      Scanner datos2 = new Scanner(System.in);
            System.out.println("que tipo de empleado quieres eliminar\n 1.diseñadores\n2.programadores");
           int eliminar=datos2.nextInt();
           datos2.nextLine();
           if(eliminar==1){
               System.out.println("elige un diseñador");
               int id=datos2.nextInt();
               listaEmpleados.remove(id);
               System.out.println("se elimino el diseñador con id"+id);
              
               
               
           }else if(eliminar==2){
               System.out.println("elige un programador");
               int id=datos2.nextInt();
             listaEmpleados.remove(id);
               System.out.println("se elimino el programador con id"+id);
           }
           return listaEmpleados;
       }
         
     public static void menu(){
         Scanner dato=new Scanner(System.in);
        //creamos un hashmap vacio para llamarlo en cada metodo
        HashMap<Integer, Empleados> listaEmpleados= new HashMap<>();
        // llenarLista(listaEmpleados);
        // consultarEmpleado(listaEmpleados);
         //agregarEmpleado(listaEmpleados);
         //eliminarEmpleado(listaEmpleados);
         //System.out.println("");


           System.out.println("1.consultar empleados\n 2.agregar Empleados\n 3.eliminar empleados\n4. salir");
           int x=dato.nextInt();
           switch(x){
               case 1:
               consultarEmpleado(listaEmpleados);
               menu();
                
                break;
               case 2: 
                   agregarEmpleado(listaEmpleados);
                   menu();
                   break;
               case 3:
                   eliminarEmpleado(listaEmpleados);
                   menu();
                   break;
               case 4:
                   break;
               
           }
       }
     }


    
    

