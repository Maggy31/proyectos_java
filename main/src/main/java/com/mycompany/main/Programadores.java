/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.main;

import java.util.ArrayList;
import java.util.Collections;
public class Programadores extends Empleados implements InterfazEmpleados{
   
    
    private String Herramienta_desarrollo="eclipse";
    private ArrayList<String> actividadesDiarias = new ArrayList<>();
    //public Programadores(){

    public Programadores() {
    }

    public Programadores(int idEmpleado, String nombre, String apellido, String tipoEmpleado, Integer edad, String codigo_empleado,String Herramienta_desarrollo) {
        super(idEmpleado, nombre, apellido, tipoEmpleado, edad, codigo_empleado);
    

        setTipoEmpleado("programador");
        
        setHerramienta_desarrollo("Eclipse");
        setCodigo_empleado("9000");
        
     this.Herramienta_desarrollo=Herramienta_desarrollo;
              actividadesDiarias.add ("analisis de requerimiento");
             actividadesDiarias.add("desarrollo de requerimientos");
                     
              actividadesDiarias.add("presentacion-prueba unitarias");
            
      
    
    }
    public String getHerramienta_desarrollo() {
        return Herramienta_desarrollo;
    }

    public void setHerramienta_desarrollo(String Herramienta_desarrollo) {
        this.Herramienta_desarrollo = Herramienta_desarrollo;
    }

    public ArrayList<String> getActividadesDiarias() {
        return actividadesDiarias;
    }

    public void setActividadesDiarias(ArrayList<String> actividadesDiarias) {
        this.actividadesDiarias = actividadesDiarias;
    }
    
    
    
    
    
   
    
 
    

    @Override
    public void obtener_actividades() {
        Collections.sort(actividadesDiarias, Collections.reverseOrder());
        for(String uno:actividadesDiarias){
            System.out.println(uno);
        }
     
    }
    public void imprimir(){
        System.out.println("nombre "+getNombre()+"\n" + "apellido " + getApellido()+"\n"+"edad "+getEdad());
    
        
        
    }

   @Override
   public String toString() {
        
       return  
    "-----------------------------\n"
              +"|          Empleados        |\n"
              +"|    nombre: " + getNombre()+    "|\n "
              +"|    apellido: " + getApellido()+"|\n"
              +"| tipoEmpleado:  "+getTipoEmpleado()+"|\n"
              +"|         edad: " + getEdad() +     "\n"
              +"| actividades_diarias: " + actividadesDiarias +"\n"
              +"|  codigo_empleado: " + getCodigo_empleado()+"\n" 
              +"Herramienta_desarrollo " + Herramienta_desarrollo+"\n"
              +"|-----------------------------------------------";
    }
     
   public void quitarCarateres(){
      String actividad = String.join(", ",actividadesDiarias);

        String actividad1= actividad.substring(1,25);
        String actividad2 = actividad.substring(28, 54);
        String actividad3 = actividad.substring(57, 85);
        
        System.out.println(actividad1 + ", " + actividad2 + ", " + actividad3 + ", ");
        
 
   } 
}   

