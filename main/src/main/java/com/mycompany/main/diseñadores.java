/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.main;
import java.util.ArrayList;
import java.util.Collections;
public class diseñadores extends Empleados implements InterfazEmpleados{
    private String herramienta_diseño;
    private ArrayList<String> actividades_diarias=new ArrayList<>();

    public diseñadores() {
    }
    
    
   
     public diseñadores(int idEmpleado,String nombre, String apellido, String tipoEmpleado, Integer edad, String codigo_empleado,String herramienta_diseño ) {
        super(idEmpleado,nombre, apellido, tipoEmpleado, edad, codigo_empleado);
        this.herramienta_diseño = herramienta_diseño;
         setTipoEmpleado("diseñador");
        setHerramienta_diseño("Photoshop");
        setCodigo_empleado("8000");
        
        actividades_diarias.add("reuniones de avance");
        actividades_diarias.add("elaboracion de diseño para paginas webs");
        actividades_diarias.add("presentacion-ajuste de diseño");
        
        if(edad <=25){
            actividades_diarias.add("revision de diseñador sr");
            
        }else{
            actividades_diarias.add("apoyo a diseñador jr");
        }
        
    }

    public void imprimir(){
    System.out.println("nombre: "+ getNombre() +"\n"+ "apellido: "+ getApellido()+"\n"+ "edad: " + getEdad());
    }

    @Override
    public String toString() {
         return "-----------------------------\n"
              +"|          Empleados        |\n"
              +"|    nombre: " + getNombre()+    "|\n "
              +"|    apellido: " + getApellido()+"|\n"
              +"| tipoEmpleado:  "+getTipoEmpleado()+"|\n"
              +"|         edad: " + getEdad() +     "\n"
              +"| Herramienta diseño " + getHerramienta_diseño() +"\n"
              +"|  codigo_empleado: " + getCodigo_empleado()+"\n" 
              +"|  actividaddes diarias"+ actividades_diarias+"\n"
              +"|-----------------------------------------------";
    
       // return "diseñadores: " + "herramienta_diseño= " + getHerramienta_diseño() +"\n"+ ", actividades_diarias= " +  + '}';
    
    }

    public ArrayList<String> getActividades_diarias() {
        return actividades_diarias;
    }

    public void setActividades_diarias(ArrayList<String> actividades_diarias) {
        this.actividades_diarias = actividades_diarias;
    }
    
    
    public String getHerramienta_diseño() {
        return herramienta_diseño;
    }

    public void setHerramienta_diseño(String herramienta_diseño) {
        this.herramienta_diseño = herramienta_diseño;
    }
    
public void obtener_actividades(){
    Collections.sort(actividades_diarias);
    for(String diseño:actividades_diarias){
        System.out.println(diseño);
    }
    
}
    
}
