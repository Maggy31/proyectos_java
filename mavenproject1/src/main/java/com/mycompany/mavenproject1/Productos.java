/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.util.Scanner;
public class Productos {
    private String IDproducto;
    private String nombre;
    private int existencia;
    private int costo;
    private int ventaDiaria;
    public Productos(){
        
    }
    public Productos(String IDproducto, String nombre, int existencia, int costo, int ventaDiaria){
      this.IDproducto=IDproducto;
      this.costo=costo;
      this.existencia=existencia;
      this.nombre=nombre;
      this.ventaDiaria=ventaDiaria;
    }

    public String getIDproducto() {
        return IDproducto;
    }

    public void setIDproducto(String IDproducto) {
        this.IDproducto = IDproducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public int getVentaDiaria() {
        return ventaDiaria;
    }

    public void setVentaDiaria(int ventaDiaria) {
        this.ventaDiaria = ventaDiaria;
    }

    @Override
    public String toString() {
        return "Productos"+"\n" + "IDproducto: " + IDproducto+"\n" + "nombre: " + nombre +"\n"+ "existencia: " + existencia+"\n" + "costo: " + costo +"\n "+" venta Diaria" + ventaDiaria+"\n";
    }
    
    
    public void venta(int cantidadProducto){
    
   int monto;
   System.out.println("numero de productos antes de la venta: " + this.existencia);
    this.existencia =this.existencia - cantidadProducto;
    System.out.println("la nueva existencia de productos: "+ this.existencia);
      monto = cantidadProducto*this.costo;
       this.ventaDiaria= this.ventaDiaria+monto; 
       System.out.println("el costo total de la venta es: " + monto);
       
    }
    
    public void ventaDiaria(){
       System.out.println("tu venta final es: "+ this.ventaDiaria);
        
    }
    public void recepcionProducto(int productosRecibidos){
        
        System.out.println("existencia anterior: "+this.existencia);
        this.existencia= this.existencia+productosRecibidos;
        System.out.println("existencia actual: "+this.existencia);
        
    }
    public void actualiza_costo(int nuevoCosto){
       
      System.out.println("nombre del producto: "+this.nombre +"\n"+"Costo anterior: "+this.costo);
     this.costo=nuevoCosto;
     System.out.println("costo actual: "+ this.costo);
     
    }
    public void menu(){
        
        System.out.println("producto: "+"\n"+ "1. venta: " +"\n"+ "2. venta diaria: "+ "\n"+ "3. recepcion de productos: "+"\n"+ "4. actualizar costo: "+"\n"+"5. salir");
       
        System.out.println("ingresa una opcion: ");
        Scanner entrada = new Scanner(System.in);
        //cree mi excepcion en el scanner
        try{
         int x =entrada.nextInt();
        entrada.nextLine();
        switch (x){
            case 1:
                System.out.println("ingresa numero de productos: ");
                int productos=entrada.nextInt();
                entrada.nextLine();
                venta(productos);
                menu();
                break;
           
            case 2:
                System.out.println("tu  venta final  es: ");
                ventaDiaria();
                menu();
                break;
                 case 3:
                System.out.println("ingresa los productos recibidos");
                int producto1=entrada.nextInt();
                entrada.nextLine();
                System.out.println("tu total de productos es: " );
                recepcionProducto(producto1);
                
                menu();
                break;
                 case 4:
                     System.out.println("ingresa el costo" );
                     int producto2=entrada.nextInt();
                    System.out.println("ingresa el nuevo costo");
                    actualiza_costo(producto2);
                    
               
                menu();
                break;
            case 5:
                System.out.println("salir");
                break;
                
        }
        } catch (java.util.InputMismatchException e){
            System.out.println("Error: valor no válido. ingresa un numero  " +e);
           
        }
        menu();
         
    }
    
}
