/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.suma;

import java.util.ArrayList;
 
public class Suma {

    public static void main(String[] args) {
        ArrayList <String> numeros=new ArrayList<>();
        numeros.add("15");
        numeros.add("30");
        numeros.add("12");
        numeros.add("60");
        System.out.println(numeros);
        
        Integer num=0;
        for(String a:  numeros){
            Integer b= Integer.valueOf(a);
            num =num + b;
        }
        System.out.println(num);
      
    }
}
